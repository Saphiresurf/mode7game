local maxdepth = 16

function indent(depth)
   local s = ''
   for i=0,depth do s=s..'  ' end
   return s
end

function dump(table, label, depth)
   local depth = depth or -1
   local label = label or tostring(table)

   print(indent(depth)..label..' {')

   for k,v in pairs(table) do
      if type(v) == 'table' and depth<maxdepth then
         dump(v, k, depth+1)
      elseif depth<maxdepth then
         print(indent(depth+1)..k..': '..tostring(v))
      else
         return
      end
   end
   if getmetatable(table) then
      dump(getmetatable(table), '__meta__', depth+1)
   end

   print(indent(depth)..'}')
end

