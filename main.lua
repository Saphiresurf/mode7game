-- main.lua --


-- Load the necessary libraries
Object = require "libraries/classic"                    -- OOP Support for Lua
HC = require "libraries/HC"                             -- Collision Detection
sti = require "libraries/sti"                           -- Support for Tiled Maps
Gamestate = require "libraries/hump/hump_gamestate"     -- Gamestates
gamera = require "libraries/gamera"                     -- Movable camera/viewport
push = require "libraries/push"                         -- Consistent resolution despite window rescaling
PM = require "libraries/playmat"                        -- Mode7 Graphics Support
anim8 = require "libraries/anim8"

-- Debug tools
require "libraries/util"

-- Pass in an instance of hardon collider (HC) shared between files
collider = require ("src/hardoncollider")


-- Other project files included here
require "src/entities/entity"
require "src/entities/player"           -- Player class
require "src/world"                     -- World builder/management

-- Default asset paths
img = "assets/img/"
json = "assets/img/animationdata/"
audio = "assets/audio/"
music = "assets/audio/music/"

-- Set default image filter to nearest neighbor
love.graphics.setDefaultFilter('nearest', 'nearest')

-- Initialize world
level1 = World("levels/test_level.lua")



function love.update(dt)

    level1:update(dt)

end

function love.draw()

    level1:draw()

end
