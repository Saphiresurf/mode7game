local collider = require "src/hardoncollider"       -- An instance of our collider library
require "src/entityinteraction/controls"            -- Takes in input and returns variables for camera rotation and entity direction
require "src/utilities/circles"                     -- Utilities for working with circles
require "src/utilities/parseanimation"           -- Used to parse animation data based on the visuals file name

Entity = Object:extend()


function Entity:new(x, y, w, h, visualName)
    -- TODO: Make a table of animation frames that were cut from a spritesheet from aseprite
    -- Variable visualname is the name of the files for this entitiys visuals
    self.visualName = visualName

    -- Get the animation from parseanimationdata.lua
    self.animation = parseAnimation(self.visualName)


    -- Characters coordinates in the level
    self.x = x
    self.y = y

    -- Velocity
    self.velocityY = 0
    self.velocityX = 0

    -- Speed of Entity
    self.speed = 85

    -- Shape of the entitys collision box
    self.width = w / 2
    self.height = h / 2

    -- Rotation of entity (setup in parselevel)
    -- 0 radians is straight ahead
    self.rotation = 0
    self.rotationSpeed = 6

    -- Entitys collision box
    self.collisionShape = collider:rectangle(self.x, self.y, self.width, self.height)

    -- THIS LINE IS REQUIRED FOR EVERY ENTITY MADE FROM THIS CLASS THAT HAS A COLLISSION
    -- Give the id the name of the entity
    -- self.collisionShape.id = "entity"
end


function Entity:update(dt)

    -- Moves the Entity according to controls set in controls.lua
    self:move(dt)

    self:updatePhysics(dt)

    -- Check entity collisions, handle them accordingly as defined
    self:checkCollisions(dt)

    -- This keeps the rotation variable from being equal to more than circle (more than 2 * pi essentially)
    -- This also stops the variable from equalling anything less than 0. It just resets to 360 and subtracts the difference.
    self.rotation = radiansLimit(self.rotation)

    -- Animation update
    self.animation:update(dt)

end


function Entity:draw(camera)
    -- Draw the current frame of animation to the screen
    PM.putSprite(camera, self.animation:getFrameInfo(self.x, self.y))
end





-- General purpose functions

-- Moves the Entity according to controls set in controls.lua
function Entity:move(dt)

    -- xaxis, yaxis, theta = update(self.rotation)

    -- Move the entity left/right
    self.velocityX = self.velocityX + (xaxis * self.speed) * dt

    -- Move the entity up/down
    self.velocityY = self.velocityY + (yaxis * self.speed) * dt

    -- Adds the desired rotation increase/decrease to entitys rotation
    self.rotation = self.rotation + (theta * self.rotationSpeed) * dt
end

-- Handles entity gravity, moves collision box to entity locatoin
function Entity:updatePhysics(dt)
    -- Gravity check 1... 2... 3... is this thing even on?
    --if (self.velocityY <= self.gravity) then
    --    self.velocityY = self.gravity
    --end

    -- Applying velocity to the entitys position so we can have some actual movement
    self.x = self.x + self.velocityX
    self.y = self.y + self.velocityY

    -- Moves the camera to the entitys position + half of the entitys width
    -- This way the camera has the entity centered
    self.collisionShape:moveTo(self.x + (self.width / 2), self.y + (self.height / 2))
    self.velocityY, self.velocityX = 0, 0
end

-- Check collisions and act accordingly
function Entity:checkCollisions(dt)

    -- Checks a list of collisions with the entity object
    for shape, collisionVector in pairs(collider:collisions(self.collisionShape)) do

        -- Objects the entity can't pass through
        -- TODO: Make platform a more general phrase. Maybe something like "collidable", but perhaps that could be a bit strange.
        if shape["id"] == "wall" then
            self.x, self.y = self.x + collisionVector.x , self.y + collisionVector.y

        end

    end

end
