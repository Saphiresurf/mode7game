local collider = require "src/hardoncollider"       -- An instance of our collider library
require "src/entityinteraction/controls"            -- Takes in input and returns variables for camera rotation and player direction
require "src/utilities/circles"                     -- Utilities for working with circles

Player = Entity:extend()


function Player:new(x, y, w, h, visualName)
    -- Picture
    -- TODO: Make a table of animation frames that were cut from a spritesheet
    --self.spriteIdle = love.graphics.newImage("/assets/img/img_player_idle.png")

    -- Sets up player coordinates, width, height, and collision box
    Player.super.new(self, x, y, w, h, visualName)

    -- Velocity
    self.velocityY = 0
    self.velocityX = 0

    -- Speed of Player
    self.speed = 85

    -- Rotation of player (setup in parselevel)
    -- 0 radians is straight ahead
    self.rotation = 0
    self.rotationSpeed = 6

    -- Players collision box ID
    self.collisionShape.id = "player"
end


function Player:update(dt)
    -- Hey for this lets make it so that entity expects an xaxis, yaxis variable (or something like that)
    -- This way controls are done in the child class, but movement is handled in the parent class
    Player.super.update(self, dt)

end


function Player:draw(camera)
    -- Draws the player to the screen
    Player.super.draw(self, camera)
end





-- General purpose functions

-- Moves the Player according to controls set in controls.lua
function Player:move(dt)
    xaxis, yaxis, theta = updateControls(self.rotation)

    -- Move the player left/right
    self.velocityX = self.velocityX + (xaxis * self.speed) * dt

    -- Move the player up/down
    self.velocityY = self.velocityY + (yaxis * self.speed) * dt

    -- Adds the desired rotation increase/decrease to players rotation
    self.rotation = self.rotation + (theta * self.rotationSpeed) * dt
end

-- Handles player gravity, moves collision box to player locatoin
function Player:updatePhysics(dt)
    -- Gravity check 1... 2... 3... is this thing even on?
    --if (self.velocityY <= self.gravity) then
    --    self.velocityY = self.gravity
    --end

    -- Applying velocity to the players position so we can have some actual movement
    self.x = self.x + self.velocityX
    self.y = self.y + self.velocityY

    -- Moves the camera to the players position + half of the players width
    -- This way the camera has the player centered
    self.collisionShape:moveTo(self.x + (self.width / 2), self.y + (self.height / 2))
    self.velocityY, self.velocityX = 0, 0
end

-- Check collisions and act accordingly
function Player:checkCollisions(dt)

    -- Checks a list of collisions with the player object
    for shape, collisionVector in pairs(collider:collisions(self.collisionShape)) do

        -- Objects the player can't pass through
        if shape["id"] == "wall" then
            self.x, self.y = self.x + collisionVector.x , self.y + collisionVector.y

        end

    end

end
