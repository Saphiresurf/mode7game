-- Make any drawing layers normally
-- Collisions called collilsions
-- Entities called entitySpawns
-- Player named Player
-- Collisions named Platform (will changes soon.... on todo)
-- Physics for level in "physics" object in spawn layer (gravity)


function parseLevel(levelPath)
    -- Grabbing the level file
    local level = sti(levelPath)

    tileSetup(level)

    level = entitySetup(level)

    return level

end


-- Creates collision boxes in accordance to the collisions layer
function tileSetup(level)
    -- ADDING TILE COLLISIONS TO A NEW CUSTOM LAYER --

    -- Create a custom layer to store our tile collison boxes in
    tiles = level:addCustomLayer("tiles", 8)

    -- Create a table to store those collisionShapes in
    tiles.collisionShape = {}

    -- Creating level collisions (platforms)
    for x, object in pairs(level.layers.collisions.objects) do
        if object.type == "wall" then
            tiles.collisionShape[x] = collider:rectangle(object.x, object.y, object.width, object.height)
            tiles.collisionShape[x] = tiles.collisionShape[x]:rotate(object.rotation)
            tiles.collisionShape[x].id = "wall"
        end
    end


end



-- Sets up entities in accordance to the entitySpawns layer
function entitySetup(level)
    -- ADDING ENTITES TO A NEW CUSTOM LAYER & DEFINING UPDATE AND DRAW FUNCTIONS--

    -- Create new custom layer called "Sprites" as the 8th layer
    local  entities = level:addCustomLayer("entities", 9)

    -- Create entity objects (player, enemeis, etc) in entities custom layer
    -- x, y coordinate fetched from object layer "entitySpawns" in level
    for k, object in pairs(level.layers.spawn.objects) do
        if object.name == "player" then
            entities.player = Player(object.x, object.y, object.width, object.height, object.properties.animationFileName)

        -- Object containing all world physics info (initialDirection, stuff like that)
        elseif object.name == "physics" then
            entities.physics = object.properties

        end

    end

    -- Give the apppropriate physics properties to the player
    -- Sets the players rotation based on the maps initial player direction (initDirection)
    entities.player.rotation = entities.physics.initDirection


    -- Override the function to update entities
    entities.update = function(self, dt)
        self.player:update(dt)
    end

    -- Override the function to draw the entities
    entities.draw = function(self)
        self.player:draw()
    end

    -- Remove uneeded object layer
    level:removeLayer("spawn")

    return level

end
