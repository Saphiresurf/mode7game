function playerValues(player, enable)
    if (enable) then
        love.graphics.setNewFont(14)
        love.graphics.print("x: " .. player.x, 10, 0)
        love.graphics.print("y: " .. player.y, 10, 14)
        love.graphics.print("animationFileName: " .. player.visualName, 10, 28)
    end
end
