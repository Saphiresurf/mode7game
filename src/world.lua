World = Object:extend()

-- Files with functions to include
require "src/parselevel"
require "src/utilities/converter"
require "src/debugtools/playerdebug"
require "src/entityinteraction/camera"

-- Shared objects to include
local collider = require "src/hardoncollider"


function World:new(levelPath)

    -- Enables or disables debug settings
    debug = false

    -- Set's up the passed in level as well as a table of entities
    self.level = parseLevel(levelPath)

    -- Independent variable for the layer filled with entities (player and such)
    self.entities = self.level.layers.entities

    -- The height and width of the new level
    self.levelWidth, self.levelHeight = self.level.width * self.level.tilewidth, self.level.height * self.level.tileheight
    self.cameraWidth, self.cameraHeight = 800, 600

    -- Create a camera for Playmat (Mode7)
    self.camera = PM.newCamera(self.cameraWidth, self.cameraHeight)

    -- Set the STI drawable area to the whole map
    -- TODO: Find a decent rendering area so that we can load only what we need instead of doing this
    self.level:resize(self.levelWidth, self.levelHeight)

    -- Create a canvas to draw the contents of Tiled to
    self.map = love.graphics.newCanvas(self.levelWidth, self.levelHeight)


end


function World:update(dt)

    -- Update every layer in the map level
    self.level:update(dt)

    -- The entities layer gets a seperate call
    -- For some reason STI doesn't want to call it with the rest of the level
    -- But it actually works out, so it might be bad if level:update() every calls it ;;
    self.level.layers.entities:update(dt)

    self.camera = followEntity(self.entities.player, self.camera)


end


function World:draw()

    -- Draw our map to a canvas
    -- This way Playmat will be able to work with it
    love.graphics.setCanvas(self.map)
        self.level:draw()
    love.graphics.setCanvas()

    -- The entities layer gets a seperate call
    -- For some reason STI doesn't want to call it with the rest of the level
    -- But it actually works out, so it might be bad if level:draw() ever decides to call it ;;
    for i, object in ipairs(self.entities) do
        self.camera = object:update(self.camera)
    end

    -- Draw the map to a Mode7 plane
    PM.drawPlane(self.camera, self.map)

    PM.renderSprites(self.camera)


    -- Debug stuff
    playerValues(self.entities.player, debug)

end
