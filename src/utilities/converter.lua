
-- converter for radians to degrees
function radiansToDegrees(radians)
    return (radians * math.pi) / 180
end

function degreesToRadians(degrees)
    return 180 / (degrees * math.pi)
end
