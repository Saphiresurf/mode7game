jsonParser = require "libraries/json"

function parseAnimation(visualName)

    -- Parse the animation data provided by aseprite
    animData = jsonParser.decode(love.filesystem.read(json .. visualName .. ".json"))

    -- Create a table for each frame of animations duration
    frameDurations = {}
    for x, object in pairs(animData.frames) do
        frameDurations[x] = object.duration
    end

    -- Retrieve the spritesheet
    spriteSheet = love.graphics.newImage(img .. visualName .. ".png")

    -- Split up the sprite into frames
    -- Always export with 0 space between frames or eLSE YOU BREAK ME WHY WOULD YOU DO THAT ???? (thank you if you didn't tho)
    frames = anim8.newGrid(animData.meta.size.w, animData.meta.size.h, spriteSheet:getWidth(), spriteSheet:getHeight())

    -- Create the animation
    return anim8.newAnimation(frames, frameDurations)
end





--[[
This is the format of a JSON table from aseprite


table: 0x12ab1fe0 {
  frames {
    1 {
      filename: trash running sprite 0.ase
      spriteSourceSize {
        y: 0
        x: 0
        h: 16
        w: 16
      }
      rotated: false
      trimmed: false
      duration: 100
      sourceSize {
        h: 16
        w: 16
      }
      frame {
        y: 0
        x: 0
        h: 16
        w: 16
      }
    }
    2 {
      filename: trash running sprite 1.ase
      spriteSourceSize {
        y: 0
        x: 0
        h: 16
        w: 16
      }
      rotated: false
      trimmed: false
      duration: 100
      sourceSize {
        h: 16
        w: 16
      }
      frame {
        y: 0
        x: 16
        h: 16
        w: 16
      }
    }
  }
  meta {
    layers {
      1 {
        blendMode: normal
        name: Layer 1
        opacity: 255
      }
    }
    format: RGBA8888
    version: 1.2-beta9
    frameTags {
    }
    app: http://www.aseprite.org/
    scale: 1
    size {
      h: 16
      w: 32
    }
    image: C:\Users\Sean\Programming\LOVE\jsonstuff\assets\img\trashrunningsprite.png
  }
}
]]--
