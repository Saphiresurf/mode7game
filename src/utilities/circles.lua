function degreeLimit(degrees)

    -- Makes it so once degrees gets past 360 that the count resets back to 0
    if degrees >= 360 then
        degrees = degrees - (360 * (math.floor(degrees % 360)))

    -- If degrees gets to be less than 0 then it subtracts it's difference from 360
    elseif degrees < 0 then
        degrees = 360 + degrees
    end

    return degrees
end

function radiansLimit(radians)
    limit = 2 * math.pi
    if radians >= limit then
        radians = radians - (limit * (math.floor(radians % limit)))

    -- If degrees gets to be less than 0 then it subtracts it's difference from 360
    elseif radians < 0 then
        radians = limit + radians
    end

    return radians
end
