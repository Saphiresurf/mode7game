function updateControls(theta)
    -- We're going to be working with spherical coordinates here to make it easier
    -- Spherical coordinates are (radius (how far ahead of the camera do we want to be), theta (camera's yaw), pheta (camera's pitch))
    -- Since we're in Mode7, though, we only need to care about (radius, theta)
    -- All we have to work with is (radius, theta)
    -- Solution sourced from second answer: http://stackoverflow.com/questions/16201573/how-to-properly-move-the-camera-in-the-direction-its-facing
    -- Relevant Wikipedia article on spherical coordinates and how to

    -- Radius is how far ahead the player is looking to move forward in fronth of the camera (speed)

    --Sets the xaxis and yaxis to 0 so that if no key is pressed it will remain at 0 (no movement)
    xaxis   = 0
    yaxis   = 0
    rotationVector = 0

    -- Relative direction we want to go in (forward or back)
    radius  = 0

    -- These control forward and backward motion
    if (love.keyboard.isDown("up")) then
        radius = 1
    end

    if (love.keyboard.isDown("down")) then
        radius = -1
    end


    -- Theta is the angle in which the camera is rotated

    xaxis = radius * math.cos(theta)
    yaxis = radius * math.sin(theta)


    -- These control the cameras rotation
    -- Theta is now being used for the rotation difference that's desired
    -- It gets added to the players rotation (this is a theta value to add to the players theta (rotation) value)
    if (love.keyboard.isDown("right")) then
        rotationVector = 0.5
    end

    if (love.keyboard.isDown("left")) then
        rotationVector = -0.5
    end

    return xaxis, yaxis, rotationVector


end

-- NOTE: Camera stuff
-- player.speed is the radius
-- camera.getRotation is the cameras theta
-- camera.getOffset() is the cameras z axis position
-- camera.setFov(fov) is actually the fov
