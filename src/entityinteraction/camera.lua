function followEntity(entity, camera)
    -- Set the position of the camera to the player
    camera:setPosition(entity.x, entity.y)

    -- For some raising the camera is rotated the wrong way by default
    camera:setRotation(entity.rotation)

    return camera
end
